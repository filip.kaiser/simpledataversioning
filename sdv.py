import os
import json
import pickle

import git
from google.cloud import storage
import pandas as pd



def upload_blob(bucket_name: str, source_file_name: str, destination_blob_name: str):
    """
    Upload a file from a local environment to a Google Storage bucket.  
    
    """
    
    storage_client = storage.Client()
    
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)   
    
    print(f'--> {source_file_name} <-- uploaded to gs://{bucket_name}/{destination_blob_name}')
    
    
    
def download_blob(bucket_name: str, source_blob_name: str, destination_file_name: str):
    """
    Download a file from a Google Storage bucket to a local environment.
    """
    
    storage_client = storage.Client()
    
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(destination_file_name)
    
    print(f'--> {destination_file_name} <-- downloaded from gs://{bucket_name}/{source_blob_name}')

    
    
def create_json(json_name: str, root_folder: str, metadata_dict: dict, bucket_name: str):
    """
    Create .json file with metadata.
    """
    
    data = {}
    data['newest_version'] = 0
    data['root_folder'] = root_folder
    data['metadata'] = []
    
    metadata_dict['commit_hash'] = git.Repo(search_parent_directories=True).head.object.hexsha
    metadata_dict['version'] = data['newest_version']
    data['metadata'].append(metadata_dict)
    
    with open(f'{json_name}.json', 'w') as outfile:
        json.dump(data, outfile, indent=4)
    print(f'--> {json_name}.json <-- created')
    
    upload_blob(bucket_name, f'{json_name}.json', f'{json_name}.json')
    
    
    
def update_json(json_name: str, metadata_dict: dict, bucket_name: str):
    """
    Update existing .json metadata file.
    """
    
    download_blob(bucket_name, f'{json_name}.json', f'{json_name}.json')
    
    with open(f'{json_name}.json', 'r') as infile:
        data = json.load(infile)
        
    data['newest_version'] += 1
    metadata_dict['version'] = data['newest_version'] 
    metadata_dict['commit_hash'] = git.Repo(search_parent_directories=True).head.object.hexsha
    metadata_dict['version'] = data['newest_version']
    data['metadata'].insert(0, metadata_dict)
    
    with open(f'{json_name}.json', 'w') as outfile:
        json.dump(data, outfile, indent=4)        
    print(f'--> {json_name}.json <-- updated')
    
    upload_blob(bucket_name, f'{json_name}.json', f'{json_name}.json')
    
    
    
def save_csvs(df_list: list, df_name_list: list, metadata_dict: dict, json_name: str, bucket_name: str):
    """
    Push csv file onto Google Storage bucket.
    """
    
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    json_exists = storage.Blob(bucket=bucket, name=f'{json_name}.json').exists(storage_client)
    
    if not json_exists:
        create_json(json_name, json_name, metadata_dict, bucket_name)
    else:
        update_json(json_name, metadata_dict, bucket_name)
        
    with open(f'{json_name}.json', 'r') as infile:
        data = json.load(infile)    
    root_folder = data['root_folder']
    version = data['newest_version']
    for df_name, df in zip(df_name_list, df_list):
        save_path = f'gs://{bucket_name}/{root_folder}/{str(version)}/{df_name}.csv'
        df.to_csv(save_path)
        print(f'--> {df_name}.csv <-- saved in {save_path}')
        
        
def save_pickles(object_list: list, pickle_path_list: list, metadata_dict: dict, json_name: str, bucket_name: str, delete_local=False):
    """
    Push pickle files onto Google Storage bucket.
    """
    
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    json_exists = storage.Blob(bucket=bucket, name=f'{json_name}.json').exists(storage_client)
    
    if not json_exists:
        create_json(json_name, json_name, metadata_dict, bucket_name)
    else:
        update_json(json_name, metadata_dict, bucket_name)
        
    with open(f'{json_name}.json', 'r') as infile:
        data = json.load(infile)    
    root_folder = data['root_folder']
    version = data['newest_version']
    for pickle_path, object_ in zip(pickle_path_list, object_list):
        pickle.dump(object_, open(f'{pickle_path}.pkl', 'wb'))
        save_path = f'gs://{bucket_name}/{root_folder}/{str(version)}/{pickle_path}.pkl'
        upload_blob(bucket_name, f'{pickle_path}.pkl', f'{root_folder}/{str(version)}/{pickle_path}.pkl')
        if delete_local:
            os.remove(f'{pickle_path}.pkl')